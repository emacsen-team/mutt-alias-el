(define-package "mutt-alias"
  "1.4" "lookup and insert expanded mutt mail aliases"
  'nil
  :keywords '("convenience")
  :url "https://github.com/davep/mutt-alias.el/")
